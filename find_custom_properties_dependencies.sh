CMD='psql --tuples-only --no-align -U qradar -c'
red="\x1b[31m"
col2="\x1b[32m"
green="\x1b[32m"
col3="\x1b[33m"
yellow="\x1b[33m"
blue="\x1b[34m"
blue="\x1b[35m"
rst="\x1b[0m"

function log(){
	echo -e "$green[+] $@$rst"
}

function debug(){
	echo -e "$blue[-] $@$rst"
}

function highlight(){
	echo -en "\x1b[1m$@$rst"
}

for property in "$@"
do
	echo -e  "${yellow}[+]Finding dependencies for $(highlight $property)$rst"
	prop=$property
	property=$(echo $property|sed s/"_"/"\\\\\_"/g)

	#Get the offense mapping for the property
	offenseMapping=$($CMD "SELECT id FROM offense_type WHERE default_label = '$prop'")

	log "Looking for dependencies in $(highlight custom rules and building blocks)"
	$CMD "SELECT rule_data FROM custom_rule WHERE encode(rule_data,'escape') LIKE '%$property%' OR encode(rule_data , 'escape') ILIKE '%<limiter%hostType=%$property%' OR encode(rule_data , 'escape') ILIKE '%offenseMapping=\"$offenseMapping\"%';" | sed s/".*<name>\(.*\)<\/name><notes>.*"/"\1"/ #grep -o "<name>.*\?</name><notes>"|cut -f2 -d '>'|awk -F "</name" '{print $1;}'|sort

	log "Looking for dependencies in $(highlight saved searches)"
	$CMD "\pset pager off" -c "SELECT itemname FROM customviewparams WHERE encode(queryparams,'escape') LIKE '%$property%' ORDER BY itemname;"|grep -v '\(Pager usage is off\.\|^\s*$\)'

	log "Looking for dependencies in $(highlight AQL properties and calculated properties)"
	$CMD "\pset pager off"  -c "SELECT propertyname FROM ariel_aql_property WHERE encode(expression,'escape') LIKE '%$property% ORDER BY propertyname' UNION ALL SELECT propertyname FROM ariel_calculated_property WHERE encode(expression,'escape') LIKE '%$property%' ORDER BY propertyname;"|grep -v '\(Pager usage is off\.\|^\s*$\)'

	log "Looking for dependencies in $(highlight offenses)"
	$CMD "\pset pager off" -c "SELECT offense.id FROM offense LEFT JOIN offense_type ON offense_mapper = offense_type.id WHERE default_label = '$prop' ORDER BY offense.id;"|grep -v '\(Pager usage is off\.\|^\s*$\)'
	
	log "Looking for dependencies in $(highlight offense saved searches)"
	$CMD "\pset pager off" -c "SELECT query FROM saved_search WHERE CAST(query AS VARCHAR(1000)) LIKE CONCAT('%offenseTypet\\\\000\\\\002',(SELECT id FROM offense_type WHERE default_label='$prop'),'t\\\\000\\\\015offenseSource%') ORDER BY id DESC;"|awk -F '&&' '{print $1}'|sed s/"\\\[[:digit:]]\+"/" "/g|grep -o "[^[:space:]]\{2,\}"|tail -n +5|grep -v '\(Pager usage is off\.\|^\s*$\)'

	log "Looking for dependencies in $(highlight forwarding profiles)"
	$CMD "\pset pager off" -c "SELECT name FROM forwarding_profile WHERE encode(xml,'escape') LIKE '%<tag>$property%' ORDER BY name;"|grep -v 'Pager usage is off.'
done
